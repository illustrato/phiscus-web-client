//#####
//##### Find in Array Object
//##### @var obj: Object
//##### @var prop: Property of object
//##### @var comp: Compare value
export function SearchObject(obj, prop, comp) {
  return obj[prop] === comp;
}

export function UpperFirst(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}
