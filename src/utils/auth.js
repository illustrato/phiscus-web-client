import { isBlank } from "@/utils/validate";
import Cookies from "js-cookie";

const TokenKey = "Token";

export function getToken() {
  return isBlank(Cookies.get(TokenKey)) ? "" : Cookies.get(TokenKey);
}

export function setToken(token) {
  return Cookies.set(TokenKey, token);
}

export function removeToken() {
  return Cookies.remove(TokenKey);
}

export function isLoggedIn() {
  return isBlank(getToken()) ? false : true;
}
