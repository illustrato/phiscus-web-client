import { isBlank } from "@/utils/validate";

export function Dynamic(response = {}) {
  if (response.type == 0) return Success(response.message, response.data);
  if (response.type == 1) return Warning(response.message, response.data);
  if (response.type == 2) return Error(response.message, response.data);
  if (response.type == 3) return Info(response.message, response.data);
}

export function Info(msg = "", data = {}) {
  return {
    type: 3,
    message: msg,
    color: "grey darken-1",
    data: data
  };
}

export function Error(msg = "", data = {}) {
  return {
    type: 2,
    message: msg,
    color: "red lighten-2",
    data: data
  };
}

export function Warning(msg = "", data = {}) {
  return {
    type: 1,
    message: msg,
    color: "yellow darken-1",
    data: data
  };
}

export function Success(msg = "", data = {}) {
  return {
    type: 0,
    message: msg,
    color: "yellow darken-1",
    data: data
  };
}

export function ThresholdColor(
  status,
  threshold = { greater: 25, lower: 10 },
  colors = { greater: "success", lower: "danger" }
) {
  if (isBlank(threshold)) threshold = { greater: 25, lower: 10 };
  if (isBlank(status)) return "";
  if (status < threshold.lower) return colors.lower;
  if (status > threshold.greater) return colors.greater;
  else return "";
}
