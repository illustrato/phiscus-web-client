/* All validations should be defined here */

export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path);
}

/**
 * Validate a valid URL
 * @param {String} textval
 * @return {Boolean}
 */
export function validURL(url) {
  const reg = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
  return reg.test(url);
}

/**
 * Validate a full-lowercase string
 * @return {Boolean}
 * @param {String} str
 */
export function validLowerCase(str) {
  const reg = /^[a-z]+$/;
  return reg.test(str);
}

/**
 * Validate a full-uppercase string
 * @return {Boolean}
 * @param {String} str
 */
export function validUpperCase(str) {
  return /^[A-Z]+$/.test(str);
}

/**
 * Check if a string contains only alphabet
 * @param {String} str
 * @param {Boolean}
 */
export function validAlphabets(str) {
  return /^[A-Za-z]+$/.test(str);
}

/**
 *
 * Alphabets only
 *
 **/
export function validAlphabetNSpace(str) {
  return /^[A-Za-z\s]+$/.test(str);
}

/**
 * Check if a string contains numbers & alphabets
 * @param {String} str
 * @param {Boolean}
 */
export function validAlphaNumeric(str) {
  return /^[a-z0-9A-Z]$/.test(str);
}

/**
 * Check if a string contains numbers & alphabets
 * @param {String} str
 * @param {Boolean}
 */
export function validAlphaNumericSpace(str) {
  return /^[a-z0-9A-Z\s]$/.test(str);
}

/**
 * Check if a string contains only numbers
 * @param {String} str
 * @param {Boolean}
 */
export function validNumeric(str) {
  return /^[0-9]\d*$/.test(str);
}

/**
 * Check if a string contains only numbers and 2 decimal points
 * @param {String} str
 * @param {Boolean}
 */
export function validDecimal(str) {
  return /^[0-9]\d*(\.\d+)?$/.test(str);
}

/**
 * Validate an email address
 * @param {String} email
 * @return {Boolean}
 */
export function validEmail(email) {
  return /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email);
}

/**
 * Is Empty / Blank
 */
export function isBlank(str) {
  return !str || /^\s*$/.test(str);
}
