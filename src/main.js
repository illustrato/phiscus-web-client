import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import "./plugins";
// import htmlToPaper from "./plugins/htmlToPaper";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";
import "cropperjs/dist/cropper.css";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  // htmlToPaper,
  render: h => h(App)
}).$mount("#app");
