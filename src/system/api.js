export default {
  uri: {
    // Authorization & Authentication
    register: "/user/register",
    login: "/user/login",
    logout: "/user/logout",
    userinfo: "/user/userinfo",

    //#####
    // A
    //#####

    // Addresses
    address_browse: "/address/browse",
    address_read: "/address/read",
    address_edit: "/address/edit",
    address_add: "/address/add",
    address_trash: "/address/trash",

    //#####
    // B
    //#####

    //#####
    // C
    //#####

    // Currency
    currency_browse: "/currency/browse",
    currency_read: "/currency/read",
    currency_edit: "/currency/edit",
    currency_add: "/currency/add",
    currency_trash: "/currency/trash",

    // Credit Card
    credit_card_browse: "/credit_card/browse",
    credit_card_read: "/credit_card/read",
    credit_card_edit: "/credit_card/edit",
    credit_card_add: "/credit_card/add",
    credit_card_trash: "/credit_card/trash",

    // Category
    category_browse: "/category/browse",
    category_read: "/category/read",
    category_edit: "/category/read",
    category_add: "/category/add",
    category_trash: "/category/trash",

    //#####
    // D
    //#####

    //#####
    // E
    //#####

    // Employee
    employee_browse: "/employee/browse",
    employee_read: "/employee/read",
    employee_edit: "/employee/read",
    employee_add: "/employee/add",
    employee_trash: "/employee/trash",

    // Entities. For app menu and individual app access, dynamically
    entity_browse: "/entity/browse",
    entity_read: "/entity/read",

    // Entities. For app menu and individual app access, dynamically
    extension_browse: "/extension/browse",
    extension_read: "/extension/read",

    //#####
    // F
    //#####

    //#####
    // G
    //#####

    //#####
    // H
    //#####

    //#####
    // I
    //#####

    //#####
    // J
    //#####

    //#####
    // K
    //#####

    //#####
    // L
    //#####

    // Language
    language_browse: "/language/browse",

    //#####
    // M
    //#####

    //#####
    // N
    //#####

    // Notifications
    notification_browse: "/notification/browse",

    //#####
    // O
    //#####

    //#####
    // P
    //#####

    // Paymethod
    paymethod_browse: "/paymethod/browse",
    paymethod_read: "/paymethod/read",
    paymethod_edit: "/paymethod/edit",
    paymethod_add: "/paymethod/add",

    // Product
    product_browse: "/product/browse",
    product_read: "/product/read",
    product_edit: "/product/read",
    product_add: "/product/add",
    product_trash: "/product/trash",

    product_relation_category: "/product/relation/category",
    product_relation_picture: "/product/relation/picture",
    product_relation_inventory: "/product/relation/inventory",
    product_relation_tax: "/product/relation/tax",

    //#####
    // Q
    //#####

    //#####
    // R
    //#####

    //#####
    // S
    //#####
    // Subscription
    subscription_browse: "/subscription/browse",
    subscription_add_card: "/subscription/add_card",
    subscription_pay: "/subscription/pay",

    // Sale
    sale_browse: "/sale/browse",
    sale_read: "/sale/read",
    sale_edit: "/sale/edit",
    sale_add: "/sale/add",

    //#####
    // T
    //#####

    // Transaction
    transaction_browse: "/transaction/browse",
    transaction_read: "/transaction/read",
    transaction_edit: "/transaction/edit",
    transaction_add: "/transaction/add",
    transaction_trash: "/transaction/trash",

    // Tax
    tax_browse: "/tax/browse",
    tax_read: "/tax/read",
    tax_edit: "/tax/read",
    tax_add: "/tax/add",
    tax_trash: "/tax/trash"

    //#####
    // U
    //#####

    //#####
    // V
    //#####

    //#####
    // W
    //#####

    //#####
    // X
    //#####

    //#####
    // Y
    //#####

    //#####
    // Z
    //#####
  }
};
