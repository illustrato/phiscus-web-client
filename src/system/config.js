///////////////////////////////////////////////
//### Necessities
////////////////////////////////////////
import store from "@/store";
import { isBlank } from "@/utils/validate";

////////////////////////////////////////////////
//### System Currency
////////////////////////////////////////
export async function SystemCurrency() {
  const response = await store.dispatch("currency/browse");
  const { data } = response;
  var result = {};
  data.filter(obj => {
    if (obj.active === true)
      result = {
        id: obj.id,
        active: obj.active,
        code: obj.code,
        description: obj.description,
        name: obj.name,
        symbol: obj.symbol
      };
  });
  if (isBlank(result)) {
    return Error("Failed to set system currency");
  } else {
    return result;
  }
}
