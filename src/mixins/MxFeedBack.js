import FeedBack from "@/components/feedback/FeedBack";

export default {
  components: {
    FeedBack,
  },
  data() {
    return {
      message: null,
      feedback: false,
      trashfeed: false // Only present when there is a delete option
    };
  },
  methods: {
    nofeedback() {
      this.trashfeed = false;
      this.feedback = false;
    }
  }
};
