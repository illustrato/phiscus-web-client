import {
  register,
  login,
  userinfo,
  about,
  usersettings,
  subscriptions,
  notification,
  language,
  currency,
  transactions,
  extensions,
  tax,
  product,
  category,
  employee,
  paymethod,
  sale
} from "@/api/data";

export function ApiCall(uri) {
  switch (uri) {
    case "/user/register":
      return register();

    case "/user/login":
      return login();

    case "/user/userinfo":
      return userinfo();

    case "/settings/about":
      return about();

    case "/settings/usersettings":
      return usersettings();

    case "/subscription/browse":
      return subscriptions();

    case "/transaction/browse":
      return transactions();

    case "/language/browse":
      return language();

    case "/currency/browse":
      return currency();

    case "/currency/read":
      var dcurr = currency();
      var rcurr = Math.floor(Math.random() * dcurr.data.length + 1);
      console.log(rcurr);
      return {
        type: 0,
        message: "Currency",
        data: [
          dcurr.data.find(
            item => item.id === rcurr //Math.floor(Math.random() * (data.data.length - 2) + 1)
          )
        ]
      };

    case "/extension/browse":
      return extensions();

    case "/extension/read":
      var exts = extensions();
      return {
        type: 0,
        message: "Extension",
        data: [exts.data[Math.floor(Math.random() * exts.data.length)]]
      };

    case "/tax/browse":
      return tax();

    case "/tax/read":
      var dtax = tax();
      return {
        type: 0,
        message: "Tax",
        data: [dtax.data[Math.floor(Math.random() * dtax.data.length)]]
      };

    case "/product/browse":
      return product();

    case "/product/read":
      var dprod = product();
      return {
        type: 0,
        message: "Product",
        data: [dprod.data[Math.floor(Math.random() * dprod.data.length)]]
      };

    case "/category/browse":
      return category();

    case "/category/read":
      var dcat = category();
      return {
        type: 0,
        message: "Category",
        data: [dcat.data[Math.floor(Math.random() * dcat.data.length)]]
      };

    case "/employee/browse":
      return employee();

    case "/employee/read":
      var demp = employee();
      return {
        type: 0,
        message: "Employee",
        data: [demp.data[Math.floor(Math.random() * demp.data.length)]]
      };

    case "/paymethod/browse":
      return paymethod();

    case "/paymethod/read":
      var dpay = paymethod();
      return {
        type: 0,
        message: "Paymethod",
        data: [dpay.data[Math.floor(Math.random() * dpay.data.length)]]
      };

    case "/sale/browse":
      return sale();

    case "/sale/read":
      var dsale = sale();
      return {
        type: 0,
        message: "Sale",
        data: [dsale.data[Math.floor(Math.random() * dsale.data.length)]]
      };

    case "/notification/browse":
      return notification();

    default:
      return "";
  }
}
