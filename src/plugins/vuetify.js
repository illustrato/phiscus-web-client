import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify);

export default new Vuetify({
  // theme: {
  //   dark: false,
  //   default: false,
  //   light: false,
  //   themes: {
  //     default: {
  //       primary: "#1976D2",
  //       secondary: "#424242",
  //       accent: "#82B1FF",
  //       error: "#FF5252",
  //       info: "#2196F3",
  //       success: "#4CAF50",
  //       warning: "#FFC107"
  //     },
  //     light: {
  //       primary: "#42a5f6",
  //       secondary: "#050b1f",
  //       accent: "#204165",
  //       error: "#FF5252",
  //       info: "#2196F3",
  //       success: "#4CAF50",
  //       warning: "#FFC107"
  //     }
  //   }
  // }
  theme: {
    dark: false,
    themes: {
      light: {
        primary: "#0b6374",
        secondary: "#050b1f",
        accent: "#204165"
      },
      dark: {}
    }
  }
});
