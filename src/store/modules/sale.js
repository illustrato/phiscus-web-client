import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  sales: [],
  sale: {}
};

const mutations = {
  SET_SALES: (state, data) => {
    state.sales = data;
  },
  SET_SALE: (state, data) => {
    state.sale = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.sale_browse, {})
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("Failed. Something went wrong. Could not get sales. You might not have any sales added");
        }
        commit(
          "SET_SALES",
          data.map(sale => {
            return sale;
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could Get sales.");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { id } = credentials;
    return http
      .get(uri.sale_read, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("No sale to fetch. Something went wrong");
        }
        commit("SET_SALE", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not find sale.");
      });
  },
  ////////////////////////////////////////////////
  //### Edit
  ////////////////////////////////////////
  edit({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.sale_edit, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update sale");
      });
  },
  ////////////////////////////////////////////////
  //### Add
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  add({}, credentials) {
    return http
      .post(uri.sale_add, credentials)
      .then(response => {
        return response;
      })
      .catch(() => {
        return Error("Failed. Something went wrong trying to add sale.");
      });
  },
  ////////////////////////////////////////////////
  //### Trash
  ////////////////////////////////////////
  trash({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.sale_trash, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not move sale to trash. Verify if item is valid and moved to trash");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not move sale to Trash. Check if it was properly loaded and refresh or press F5");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
