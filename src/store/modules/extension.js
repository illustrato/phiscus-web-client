import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  extensions: [],
  extension: {}
};

const mutations = {
  SET_EXTENSIONS: (state, data) => {
    state.extensions = data;
  },
  SET_EXTENSION: (state, data) => {
    state.extension = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.extension_browse, {})
      .then(response => {
        console.log(response);
        const { data } = response;
        if (!data) {
          return Info("Failed. Could find any modules.");
        }
        commit(
          "SET_EXTENSIONS",
          data.map(extension => {
            return {
              id: extension.id,
              title: extension.title,
              icon: extension.logo.icon,
              logo: extension.logo.image,
              used: extension.used,
              description: extension.description.english
            };
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could find any modules.");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { id } = credentials;
    return http
      .get(uri.extension_read, { id: id })
      .then(response => {
        const { data } = response;
        console.log(response);
        if (!data) {
          return Info("Module details could not be fetched. something went wrong");
        }
        commit("SET_EXTENSION", data[0]);
        return response;
      })
      .catch(e => {
        console.log(e);
        return Error("Failed. module details could not be fetched. something went wrong");
      });
  },
  ////////////////////////////////////////////////
  //### Edit
  ////////////////////////////////////////
  edit({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.extension_edit, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update extension");
      });
  },
  ////////////////////////////////////////////////
  //### Add
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  add({}, credentials) {
    return http
      .post(uri.extension_add, credentials)
      .then(response => {
        return response;
      })
      .catch(() => {
        return Error("Failed. Something went wrong trying to add extension.");
      });
  },
  ////////////////////////////////////////////////
  //### Trash
  ////////////////////////////////////////
  trash({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.extension_trash, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not move extension to trash. Verify if item is valid and moved to trash");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not move extension to Trash. Check if it was properly loaded and refresh or press F5");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
