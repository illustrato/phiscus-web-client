import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  notifications: []
};

const mutations = {
  SET_NOTIFICATIONS: (state, data) => {
    state.notifications = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.notification_browse, {})
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("You have no notifications");
        }
        commit(
          "SET_NOTIFICATIONS",
          data.map(notification => {
            return {
              id: notification.id,
              action: notification.action,
              headline: notification.headline,
              subtitle: notification.subtitle,
              title: notification.title
            };
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could Get attribute. Try Refreshing or press F5 or Go back to previos working page");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
