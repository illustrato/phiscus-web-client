import defaultSettings from "@/system/settings";
const { title, showSettings, tagsView, fixedHeader, sidebarLogo, theme } = defaultSettings;

const state = {
  title: title,
  theme: theme,
  drawer: false,
  sidebar: true,
  haschildren: false,
  showSettings: showSettings,
  tagsView: tagsView,
  fixedHeader: fixedHeader,
  sidebarLogo: sidebarLogo
};

const mutations = {
  CHANGE_SETTING: (state, { key, value }) => {
    if (Object.prototype.hasOwnProperty.call(state, key)) {
      state[key] = value;
    }
  },
  SET_SIDEBAR: state => {
    state.sidebar = !state.sidebar;
  },
  SET_HASCHILD: (state, credentials) => {
    state.haschildren = credentials;
  },
  SET_DRAWER: state => {
    state.drawer = !state.drawer;
  }
};

const actions = {
  changeSetting({ commit }, data) {
    commit("CHANGE_SETTING", data);
  },
  sidebar({ commit }) {
    commit("SET_SIDEBAR");
  },
  haschildren({ commit }, credentials) {
    console.log(credentials);
    commit("SET_HASCHILD", credentials);
  },
  drawer({ commit }) {
    commit("SET_DRAWER");
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
