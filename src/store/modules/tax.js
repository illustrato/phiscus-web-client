import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  taxes: [],
  tax: {}
};

const mutations = {
  SET_TAXES: (state, data) => {
    state.taxes = data;
  },
  SET_TAX: (state, data) => {
    state.tax = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.tax_browse, {})
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("Failed. fetch tax. Your database could be empty. Add a tax");
        }
        commit(
          "SET_TAXES",
          data.map(tax => {
            return {
              id: tax.id,
              name: tax.name,
              display_name: tax.display_name,
              description: tax.description,
              percentage_charge: tax.percentage_charge,
              amount_charge: tax.amount_charge
            };
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not get tax.");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { id } = credentials;
    return http
      .get(uri.tax_read, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("No tax to fetch. Something went wrong with the selected item");
        }
        commit("SET_TAX", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not find tax.");
      });
  },
  ////////////////////////////////////////////////
  //### Edit
  ////////////////////////////////////////
  edit({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.tax_edit, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update tax");
      });
  },
  ////////////////////////////////////////////////
  //### Add
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  add({}, credentials) {
    return http
      .post(uri.tax_add, credentials)
      .then(response => {
        return response;
      })
      .catch(() => {
        return Error("Failed. Something went wrong trying to add tax.");
      });
  },
  ////////////////////////////////////////////////
  //### Trash
  ////////////////////////////////////////
  trash({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.tax_trash, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not move tax to trash. Verify if item is valid and moved to trash");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not move tax to Trash. Check if it was properly loaded and refresh or press F5");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
