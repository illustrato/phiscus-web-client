import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  subscriptions: [],
  subscription: {}
};

const mutations = {
  SET_SUBSCRIPTIONS: (state, data) => {
    state.subscriptions = data;
  },
  SET_SUBSCRIPTION: (state, data) => {
    state.subscription = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.subscription_browse, {})
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("You have no Subscriptions yet");
        }
        console.log(data);
        commit(
          "SET_SUBSCRIPTIONS",
          data.map(subscription => {
            return subscription;
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not get subscriptions.");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { id } = credentials;
    return http
      .get(uri.subscription_read, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info(
            "Could not properly setup selected app. please make sure your subscription is up to date and you have access to this app."
          );
        }
        commit("SET_SUBSCRIPTION", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not get app data. Try Refreshing or Go back to previos working page");
      });
  },
  ////////////////////////////////////////////////
  //### Pay
  ////////////////////////////////////////
  pay({ commit }, credentials) {
    const { card, address, amount } = credentials;
    return http
      .post(uri.subscription_pay, {
        card: card,
        address: address,
        amount: amount
      })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info(
            "Could not properly setup selected app. please make sure your subscription is up to date and you have access to this app."
          );
        }
        commit("SET_SUBSCRIPTION", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not get app data. Try Refreshing or Go back to previos working page");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
