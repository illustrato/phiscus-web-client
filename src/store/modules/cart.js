import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error } from "@/utils/misc";
const { uri } = defaultURI;
import Paymethod from "./paymethod";

const state = {
  cart: [],
  carted: {
    paymethod: null,
    tendered: null,
    tip: null,
    discount: null,
    vat: 0,
    otherTax: 0,
    subtotal: 0,
    total: 0
  }
};

const mutations = {
  //
  PUSH_PROD_TO_CART: (state, product) => {
    product.qty = 1;
    product.charge = product.price * product.qty;
    state.cart.push(product);
  },
  //
  INCR_PROD_QTY_IN_CART: (state, item) => {
    item.qty++;
    item.charge = item.price * item.qty;
    state.cart.push();
  },
  //
  DECR_PROD_QTY_IN_CART: (state, item) => {
    if (item.qty <= 1) {
      return;
    }
    item.qty--;
    item.charge = item.price * item.qty;
    state.cart.push();
  },
  //
  REMOVE_ITEM_CART: state => {
    state.cart.push();
  },
  //
  DECR_PROD_INVENTORY: (state, product) => {
    if (product.stockable) {
      product.stock--;
    }
  },
  //
  INCR_PROD_INVENTORY: (state, product) => {
    if (product.stockable) {
      product.stock++;
    }
  },
  //
  CART_TOTALS: (state, carted) => {
    state.carted.vat = carted.vat;
    state.carted.otherTax = carted.otherTax;
    state.carted.subtotal = carted.subtotal;
    state.carted.total = carted.total;
  },
  //
  CART_PAYMETHOD: (state, paymethod) => {
    state.carted.paymethod = paymethod;
  },
  //
  CART_TENDERED: (state, tendered) => {
    state.carted.tendered = tendered;
  },
  //
  CART_TIP: (state, tip) => {
    state.carted.tip = tip;
  },
  //
  CART_DISCOUNT: (state, discount) => {
    state.carted.discount = discount;
  }
};

const actions = {
  add({ state, commit }, product) {
    if (product.stockable == "Yes" && product.stock < 1) {
      console.log("Item Ran out of Stock!!!!");
    } else if (product.stockable && product.stock > 0) {
      const item = state.cart.find(it => it.id == product.id);
      if (!item) {
        commit("PUSH_PROD_TO_CART", product);
      } else {
        commit("INCR_PROD_QTY_IN_CART", item);
      }
      commit("DECR_PROD_INVENTORY", product);
    } else {
      const item = state.cart.find(it => it.id == product.id);
      if (!item) {
        commit("PUSH_PROD_TO_CART", product);
      } else {
        commit("INCR_PROD_QTY_IN_CART", item);
      }
    }
  },
  // remove From Cart
  remove({ state, commit }, item) {
    let index = state.cart.indexOf(item);
    if (index > -1) {
      state.cart.splice(index, 1);
      commit("REMOVE_ITEM_CART");
    }
  },
  //
  decrement({ state, commit }, product) {
    const item = state.cart.find(it => it.id == product.id);
    if (item) {
      if (product.stockable && product.stock > 0) {
        commit("DECR_PROD_QTY_IN_CART", item);
        commit("INCR_PROD_INVENTORY", product);
      } else {
        commit("DECR_PROD_QTY_IN_CART", item);
      }
    }
    return;
  },
  //
  increment({ state, commit }, product) {
    const item = state.cart.find(it => it.id == product.id);
    if (item) {
      if (product.stockable && product.stock > 0) {
        commit("INCR_PROD_QTY_IN_CART", item);
        commit("DECR_PROD_INVENTORY", product);
      } else {
        commit("INCR_PROD_QTY_IN_CART", item);
      }
    }
    return;
  },
  //
  carted({ state, commit }) {
    let hold = state.carted;
    hold.vat = 0;
    hold.otherTax = 0;
    hold.subtotal = 0;
    hold.total = 0;
    state.cart.forEach(function(element) {
      hold.total += element.price * element.qty;
      // tax or extra-charge and vat
      if (element.tax.length > 0) {
        element.tax.forEach(function(el) {
          if (el.name == "vat") {
            hold.vat += parseFloat((el.percentage_charge / 100) * element.price) + parseFloat(el.amount_charge);
            hold.subtotal +=
              element.price - (parseFloat((el.percentage_charge / 100) * element.price) + parseFloat(el.amount_charge)).toFixed(2);
          } else {
            hold.otherTax += parseFloat((el.percentage_charge / 100) * element.price) + parseFloat(el.amount_charge);
            hold.subtotal +=
              element.price - (parseFloat((el.percentage_charge / 100) * element.price) + parseFloat(el.amount_charge)).toFixed(2);
          }
        });
      }
      // subtotal
      else {
        hold.subtotal += element.price * element.qty;
      }
      // Payment Method Charge
      if (hold.paymethod !== null) {
        let val = (parseFloat(hold.paymethod.percentage_charge) / 100) * element.price;
        hold.otherTax += val;
        hold.total += val;
      }
    });
    // Make Calc
    if (hold.tip > 0) {
      hold.subtotal += parseFloat(hold.tip);
      hold.total += parseFloat(hold.tip);
    }
    commit("CART_TOTALS", hold);
  },
  //
  setpaymethod({ commit }, pm) {
    const index = Paymethod.state.paymethods.find((item) => item.id == pm);
    commit("CART_PAYMETHOD", index);
  },
  settendered({ commit }, val) {
    commit("CART_TENDERED", val);
  },
  settip({ commit }, val) {
    commit("CART_TIP", val);
  },
  setdiscount({ commit }, val) {
    commit("CART_DISCOUNT", val);
  },
  ////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  create({}, credentials) {
    return http.post(uri.sale_add, credentials)
      .then((response) => {
        console.log("Created");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could Not Create Sale.");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
