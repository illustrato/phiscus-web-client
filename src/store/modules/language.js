import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  languages: [],
  language: {}
};

const mutations = {
  SET_LANGUAGES: (state, data) => {
    state.languages = data;
  },
  SET_LANGUAGE: (state, data) => {
    state.language = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.language_browse, {})
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("Could not find any supported languages");
        }
        commit(
          "SET_LANGUAGES",
          data.map(language => {
            return {
              id: language.id,
              name: language.name,
              code: language.code,
              active: language.active
            };
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could Get attribute. Try Refreshing or press F5 or Go back to previos working page");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { id } = credentials;
    return http
      .get(uri.language_read, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info(
            "Could not properly setup selected app. please make sure your subscription is up to date and you have access to this app."
          );
        }
        commit("SET_LANGUAGE", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not get app data. Try Refreshing or Go back to previos working page");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
