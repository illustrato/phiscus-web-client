import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  addresses: [],
  address: {}
};

const mutations = {
  SET_ADDRESSES: (state, data) => {
    state.addresses = data;
  },
  SET_ADDRESS: (state, data) => {
    state.address = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.address_browse, {})
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("You have no addresses yet");
        }
        console.log(data);
        commit(
          "SET_ADDRESSES",
          data.map(address => {
            return address;
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not get addresses.");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { id } = credentials;
    return http
      .get(uri.address_read, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("Failed trying to get address details.");
        }
        commit("SET_ADDRESS", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not get address details.");
      });
  },

  ////////////////////////////////////////////////
  //### Edit
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  edit({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.address_edit, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update address");
      });
  },

  ////////////////////////////////////////////////
  //### Add
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  add({}, credentials) {
    return http
      .post(uri.address_add, credentials)
      .then(response => {
        return response;
      })
      .catch(() => {
        return Error("Failed. Something went wrong trying to add address.");
      });
  },

  ////////////////////////////////////////////////
  //### Delete
  ////////////////////////////////////////
  delete({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.address_trash, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not permanently delete address");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not permanently delete address");
      });
  },

  ////////////////////////////////////////////////
  //### Trash
  ////////////////////////////////////////
  trash({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.address_trash, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not trash address");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not move address to Trash.");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
