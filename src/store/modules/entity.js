import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  entities: [],
  entity: {}
};

const mutations = {
  SET_ENTITIES: (state, data) => {
    state.entities = data;
  },
  SET_ENTITY: (state, data) => {
    state.entity = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.entity_browse, {})
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("You have no Entities in Your System. You can add them in the Market, which ever one fits your needs");
        }
        commit(
          "SET_ENTITIES",
          data.map(entity => {
            return {
              id: entity.id,
              slug: entity.slug,
              name: entity.name,
              display_singular: entity.display_singular,
              display_plural: entity.display_plural,
              icon: entity.icon,
              description: entity.description,
              permissions: entity.permissions
            };
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could Get attribute. Try Refreshing or press F5 or Go back to previos working page");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { slug } = credentials;
    return http
      .get(uri.entity_read, { slug: slug })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info(
            "Could not properly setup selected app. please make sure your subscription is up to date and you have access to this app."
          );
        }
        commit("SET_ENTITY", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not get app data. Try Refreshing or Go back to previos working page");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
