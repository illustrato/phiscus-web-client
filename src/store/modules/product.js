import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  products: [],
  product: {}
};

const mutations = {
  SET_PRODUCTS: (state, data) => {
    state.products = data;
  },
  SET_PRODUCT: (state, data) => {
    state.product = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.product_browse, {})
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("Failed. Something went wrong. Could not get products. You might not have any products added");
        }
        commit(
          "SET_PRODUCTS",
          data.map(product => {
            return product;
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could Get products.");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { id } = credentials;
    return http
      .get(uri.product_read, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("No product to fetch. Something went wrong");
        }
        commit("SET_PRODUCT", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not find product.");
      });
  },
  ////////////////////////////////////////////////
  //### Edit
  ////////////////////////////////////////
  edit({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.product_edit, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update product");
      });
  },
  ////////////////////////////////////////////////
  //### Add
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  add({}, credentials) {
    return http
      .post(uri.product_add, credentials)
      .then(response => {
        return response;
      })
      .catch(() => {
        return Error("Failed. Something went wrong trying to add product.");
      });
  },
  ////////////////////////////////////////////////
  //### Trash
  ////////////////////////////////////////
  trash({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.product_trash, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not move product to trash. Verify if item is valid and moved to trash");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not move product to Trash. Check if it was properly loaded and refresh or press F5");
      });
  },

  ////////////////////////////////////////////////
  //### Relation Category
  ////////////////////////////////////////
  relation_category({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.product_relation_category, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to categorise product");
      });
  },

  ////////////////////////////////////////////////
  //### Relation Inventory
  ////////////////////////////////////////
  relation_inventory({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.product_relation_inventory, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update product inventory");
      });
  },

  ////////////////////////////////////////////////
  //### Relation Avatar
  ////////////////////////////////////////
  relation_picture({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.product_relation_picture, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update product inventory");
      });
  },

  ////////////////////////////////////////////////
  //### Relation Taxation
  ////////////////////////////////////////
  relation_tax({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.product_relation_inventory, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update product taxation");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
