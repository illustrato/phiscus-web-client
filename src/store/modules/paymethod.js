import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  paymethods: [],
  paymethod: {}
};

const mutations = {
  SET_PAYMETHODS: (state, data) => {
    state.paymethods = data;
  },
  SET_PAYMETHOD: (state, data) => {
    state.paymethod = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.paymethod_browse, {})
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("Failed. Something went wrong. Could not get paymethods. You might not have any paymethods added");
        }
        commit(
          "SET_PAYMETHODS",
          data.map(paymethod => {
            return paymethod;
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could Get paymethods.");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { id } = credentials;
    return http
      .get(uri.paymethod_read, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("No paymethod to fetch. Something went wrong");
        }
        commit("SET_PAYMETHOD", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not find paymethod.");
      });
  },
  ////////////////////////////////////////////////
  //### Edit
  ////////////////////////////////////////
  edit({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.paymethod_edit, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update paymethod");
      });
  },
  ////////////////////////////////////////////////
  //### Add
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  add({}, credentials) {
    return http
      .post(uri.paymethod_add, credentials)
      .then(response => {
        return response;
      })
      .catch(() => {
        return Error("Failed. Something went wrong trying to add paymethod.");
      });
  },
  ////////////////////////////////////////////////
  //### Trash
  ////////////////////////////////////////
  trash({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.paymethod_trash, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not move paymethod to trash. Verify if item is valid and moved to trash");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not move paymethod to Trash. Check if it was properly loaded and refresh or press F5");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
