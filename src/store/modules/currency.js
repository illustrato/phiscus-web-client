import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  currencies: [],
  currency: {}
};

const mutations = {
  SET_CURRENCIES: (state, data) => {
    state.currencies = data;
  },
  SET_CURRENCY: (state, data) => {
    state.currency = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.currency_browse, {})
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("Failed. Something went wrong. Could not get currencies. You might not have any currencies added");
        }
        commit(
          "SET_CURRENCIES",
          data.map(currency => {
            return {
              id: currency.id,
              code: currency.code,
              name: currency.name,
              symbol: currency.symbol,
              active: currency.active,
              description: currency.description
            };
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could Get currencies.");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { id } = credentials;
    return http
      .get(uri.currency_read, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("No currency to fetch. Something went wrong");
        }
        commit("SET_CURRENCY", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not find currency.");
      });
  },
  ////////////////////////////////////////////////
  //### Edit
  ////////////////////////////////////////
  edit({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.currency_edit, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update currency");
      });
  },
  ////////////////////////////////////////////////
  //### Add
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  add({}, credentials) {
    return http
      .post(uri.currency_add, credentials)
      .then(response => {
        return response;
      })
      .catch(() => {
        return Error("Failed. Something went wrong trying to add currency.");
      });
  },
  ////////////////////////////////////////////////
  //### Trash
  ////////////////////////////////////////
  trash({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.currency_trash, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not move currency to trash. Verify if item is valid and moved to trash");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not move currency to Trash. Check if it was properly loaded and refresh or press F5");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
