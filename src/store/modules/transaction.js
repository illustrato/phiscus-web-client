import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  transactions: [],
  transaction: {}
};

const mutations = {
  SET_TRANSACTIONS: (state, data) => {
    state.transactions = data;
  },
  SET_TRANSACTION: (state, data) => {
    state.transaction = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.transaction_browse, {})
      .then((response) => {
        const { data } = response;
        if (!data) {
          return Info("You have no transactions yet");
        }
        console.log(data);
        commit(
          "SET_TRANSACTIONS",
          data.map((transaction) => {
            return transaction;
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not get transactions.");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { id } = credentials;
    return http
      .get(uri.transaction_read, { id: id })
      .then((response) => {
        const { data } = response;
        if (!data) {
          return Info("Failed trying to get transaction details.");
        }
        commit("SET_TRANSACTION", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not get transaction details.");
      });
  },

  ////////////////////////////////////////////////
  //### Edit
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  edit({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.transaction_edit, credentials)
      .then((response) => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update transaction");
      });
  },

  ////////////////////////////////////////////////
  //### Add
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  add({}, credentials) {
    return http
      .post(uri.transaction_add, credentials)
      .then((response) => {
        return response;
      })
      .catch(() => {
        return Error("Failed. Something went wrong trying to add transaction.");
      });
  },

  ////////////////////////////////////////////////
  //### Delete
  ////////////////////////////////////////
  delete({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.transaction_trash, { id: id })
      .then((response) => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not permanently delete transaction");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not permanently delete transaction");
      });
  },

  ////////////////////////////////////////////////
  //### Trash
  ////////////////////////////////////////
  trash({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.transaction_trash, { id: id })
      .then((response) => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not trash transaction");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not move transaction to Trash.");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
