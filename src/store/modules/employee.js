import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  employees: [],
  employee: {}
};

const mutations = {
  SET_EMPLOYEES: (state, data) => {
    state.employees = data;
  },
  SET_EMPLOYEE: (state, data) => {
    state.employee = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.employee_browse, {})
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("Failed. Something went wrong. Could not get employees. You might not have any employees added");
        }
        commit(
          "SET_EMPLOYEES",
          data.map(employee => {
            return {
              id: employee.id,
              avatar: employee.avatar,
              nickname: employee.nickname,
              first_name: employee.first_name,
              last_name: employee.last_name,
              is_active: employee.is_active == 1 ? "Yes" : "No",
              is_available: employee.is_available == 1 ? "Yes" : "No"
            };
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could Get employees.");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { id } = credentials;
    return http
      .get(uri.employee_read, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("No employee to fetch. Something went wrong");
        }
        commit("SET_EMPLOYEE", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not find employee.");
      });
  },
  ////////////////////////////////////////////////
  //### Edit
  ////////////////////////////////////////
  edit({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.employee_edit, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update employee");
      });
  },
  ////////////////////////////////////////////////
  //### Add
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  add({}, credentials) {
    return http
      .post(uri.employee_add, credentials)
      .then(response => {
        return response;
      })
      .catch(() => {
        return Error("Failed. Something went wrong trying to add employee.");
      });
  },
  ////////////////////////////////////////////////
  //### Trash
  ////////////////////////////////////////
  trash({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.employee_trash, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not move employee to trash. Verify if item is valid and moved to trash");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not move employee to Trash. Check if it was properly loaded and refresh or press F5");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
