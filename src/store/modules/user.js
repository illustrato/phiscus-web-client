import { resetRouter } from "@/router";
import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { getToken, setToken, removeToken } from "@/utils/auth";
import { Success, Error } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  token: getToken(),
  user: {
    id: null,
    name: null,
    fname: null,
    lname: null,
    email: null,
    phone: null,
    dob: null,
    avatar: null,
    roles: [],
    permissions: []
  }
};

const mutations = {
  SET_ID: (state, id) => {
    state.user.id = id;
  },
  SET_NAME: (state, name) => {
    state.user.name = name;
  },
  SET_FNAME: (state, name) => {
    state.user.fname = name;
  },
  SET_LNAME: (state, name) => {
    state.user.lname = name;
  },
  SET_DOB: (state, dob) => {
    state.user.dob = dob;
  },
  SET_PHONE: (state, phone) => {
    state.user.phone = phone;
  },
  SET_EMAIL: (state, email) => {
    state.user.email = email;
  },
  SET_ROLES: (state, roles) => {
    state.user.roles = roles;
  },
  SET_PERMISSIONS: (state, permissions) => {
    state.user.permissions = permissions;
  },
  SET_AVATAR: (state, avatar) => {
    state.user.avatar = avatar;
  }
};

const actions = {
  // eslint-disable-next-line no-empty-pattern
  register({}, credentials) {
    return http
      .post(uri.register, credentials)
      .then(response => {
        return response;
      })
      .catch(() => {
        return Error("Registration Failed. Perhaps your credentials were incorrect");
      });
  },

  // Login the user
  login({ commit }, credentials) {
    const { email, password, device_name } = credentials;
    return http
      .post(uri.login, {
        email: email.trim(),
        password: password,
        device_name: device_name
      })
      .then(response => {
        const { data } = response;
        if (!data) {
          resetRouter();
          return Error("Login Failed. Perhaps your credentials were incorrect");
        }
        commit("SET_EMAIL", data.user.email);
        commit("SET_NAME", data.user.fname + " " + data.user.lname);
        commit("SET_ID", data.user.id);
        setToken(data.token);
        resetRouter();
        return Success("Welcome Back.");
      })
      .catch(() => {
        return Error("Login Failed. Perhaps your credentials were incorrect");
      });
  },

  // get user info
  userinfo({ commit, state }) {
    return http
      .get(uri.userinfo, { token: state.token })
      .then(response => {
        const { data } = response;
        if (!data) {
          resetRouter();
          return Error("Please Login again. You were inactive for some time.");
        }
        if (!data.roles || data.roles.length <= 0) {
          return Error("This user does not have a role. If this is an error, please logout and login again");
        }
        commit("SET_EMAIL", data.user.email);
        commit("SET_NAME", data.user.fname + " " + data.user.lname);
        commit("SET_FNAME", data.user.fname);
        commit("SET_LNAME", data.user.lname);
        commit("SET_DOB", data.user.dob);
        commit("SET_PHONE", data.user.phone);
        commit("SET_ID", data.user.id);
        commit("SET_ROLES", data.roles);
        commit("SET_PERMISSIONS", data.permissions);
        commit("SET_AVATAR", data.user.avatar);
        return response;
      })
      .catch(() => {
        return Error("Something went wrong authenticating user details. Please refresh and try again or logout and login again.");
      });
  },

  // Logout the user
  logout({ commit }) {
    return http
      .post(uri.logout)
      .then(() => {
        commit("SET_EMAIL", null);
        commit("SET_NAME", null);
        commit("SET_ID", null);
        removeToken();
        resetRouter();
      })
      .catch(() => {
        return Error("Something went wrong could not log you out. Refresh and try again");
      });
  },

  ////////////////////////////////////////////////
  //### Edit
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  edit({}, credentials) {
    return http
      .post(uri.user_edit, credentials)
      .then(response => {
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update transaction");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
