import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  categories: [],
  category: {}
};

const mutations = {
  SET_CATEGORIES: (state, data) => {
    state.categories = data;
  },
  SET_CATEGORY: (state, data) => {
    state.category = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.category_browse, {})
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("Failed. Something went wrong. Could not get categories. You might not have any categories added");
        }
        commit(
          "SET_CATEGORIES",
          data.map(category => {
            return category;
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could Get categories.");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { id } = credentials;
    return http
      .get(uri.category_read, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("No category to fetch. Something went wrong");
        }
        commit("SET_CATEGORY", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not find category.");
      });
  },
  ////////////////////////////////////////////////
  //### Edit
  ////////////////////////////////////////
  edit({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.category_edit, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update category");
      });
  },
  ////////////////////////////////////////////////
  //### Add
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  add({}, credentials) {
    return http
      .post(uri.category_add, credentials)
      .then(response => {
        return response;
      })
      .catch(() => {
        return Error("Failed. Something went wrong trying to add category.");
      });
  },
  ////////////////////////////////////////////////
  //### Trash
  ////////////////////////////////////////
  trash({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.category_trash, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not move category to trash. Verify if item is valid and moved to trash");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not move category to Trash. Check if it was properly loaded and refresh or press F5");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
