import http from "@/services/http-common";
import defaultURI from "@/system/api";
import { Error, Info } from "@/utils/misc";
const { uri } = defaultURI;

const state = {
  cards: [],
  card: {}
};

const mutations = {
  SET_CARDS: (state, data) => {
    state.cards = data;
  },
  SET_CARD: (state, data) => {
    state.card = data;
  }
};

const actions = {
  ////////////////////////////////////////////////
  //### Browse
  ////////////////////////////////////////
  browse({ commit }) {
    return http
      .get(uri.credit_card_browse, {})
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("You have no credit cards yet");
        }
        console.log(data);
        commit(
          "SET_CARDS",
          data.map(card => {
            return card;
          })
        );
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not get credit cards.");
      });
  },
  ////////////////////////////////////////////////
  //### Read
  ////////////////////////////////////////
  read({ commit }, credentials) {
    const { id } = credentials;
    return http
      .get(uri.credit_card_read, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Info("Failed trying to get credit card details.");
        }
        commit("SET_CARD", data[0]);
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not get credit card details.");
      });
  },

  ////////////////////////////////////////////////
  //### Edit
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  edit({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.credit_card_edit, credentials)
      .then(response => {
        dispatch("browse");
        dispatch("read", { id: id });
        return response;
      })
      .catch(() => {
        return Error("Something went wrong trying to update credit card");
      });
  },

  ////////////////////////////////////////////////
  //### Add
  ////////////////////////////////////////
  // eslint-disable-next-line no-empty-pattern
  add({}, credentials) {
    return http
      .post(uri.credit_card_add, credentials)
      .then(response => {
        return response;
      })
      .catch(() => {
        return Error("Failed. Something went wrong trying to add Credit Card.");
      });
  },

  ////////////////////////////////////////////////
  //### Delete
  ////////////////////////////////////////
  delete({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.credit_card_trash, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not permanently delete credit card");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not permanently delete credit card");
      });
  },

  ////////////////////////////////////////////////
  //### Trash
  ////////////////////////////////////////
  trash({ dispatch }, credentials) {
    const { id } = credentials;
    return http
      .post(uri.credit_card_trash, { id: id })
      .then(response => {
        const { data } = response;
        if (!data) {
          return Error("Failed. could not trash credit card");
        }
        dispatch("browse");
        return response;
      })
      .catch(() => {
        return Error("Failed. Could not move credit card to Trash.");
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
