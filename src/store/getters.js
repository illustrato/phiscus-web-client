const getters = {
  //#####
  // A
  //#####
  addresses: state => state.address.addresses,
  address: state => state.address.address,

  //#####
  // B
  //#####

  //#####
  // C
  //#####
  currencies: state => state.currency.currencies,
  currency: state => state.currency.currency,

  cards: state => state.creditcard.cards,
  card: state => state.creditcard.card,

  cart: state => state.cart.cart,
  carted: state => state.cart.carted,

  categories: state => state.category.categories,
  category: state => state.category.category,

  //#####
  // D
  //#####
  drawer: state => state.settings.drawer,

  //#####
  // E
  //#####
  employees: state => state.employee.employees,
  employee: state => state.employee.employee,

  entities: state => state.entity.entities,
  entity: state => state.entity.entity,

  //#####
  // F
  //#####

  //#####
  // G
  //#####

  //#####
  // H
  //#####
  haschildren: state => state.settings.haschildren,
  //#####
  // I
  //#####

  //#####
  // J
  //#####

  //#####
  // K
  //#####

  //#####
  // L
  //#####
  languages: state => state.language.languages,
  language: state => state.language.language,

  //#####
  // M
  //#####
  extensions: state => state.extension.extensions,
  extension: state => state.extension.extension,

  //#####
  // N
  //#####
  notifications: state => state.notification.notifications,

  //#####
  // O
  //#####

  //#####
  // P
  //#####
  products: state => state.product.products,
  product: state => state.product.product,

  paymethods: state => state.paymethod.paymethods,
  paymethod: state => state.paymethod.paymethod,

  //#####
  // Q
  //#####

  //#####
  // R
  //#####

  //#####
  // S
  //#####
  sidebar: state => state.settings.sidebar,

  subscriptions: state => state.subscription.subscriptions,
  subscription: state => state.subscription.subscription,

  sales: state => state.sale.sales,
  sale: state => state.sale.sale,

  //#####
  // T
  //#####
  title: state => state.settings.title,

  transactions: state => state.transaction.transactions,
  transaction: state => state.transaction.transaction,

  taxes: state => state.tax.taxes,
  tax: state => state.tax.tax,

  //#####
  // U
  //#####
  user: state => state.user.user

  //#####
  // V
  //#####

  //#####
  // W
  //#####

  //#####
  // X
  //#####

  //#####
  // Y
  //#####

  //#####
  // Z
  //#####
};
export default getters;
