import { ApiCall } from "@/api/api";
import { getToken } from "@/utils/auth";
import axios from "axios";
import NProgress from "nprogress"; // progress bar
import "nprogress/nprogress.css"; // progress bar style

///////////////////////////////////////////////////////////////////////////////
/////////  Router Configurations
/////////  Permissions Routing
/////////  NProgress - (Loader)
/////////  Role routing Specific
/////////  Basically, Its a smart Routing system configuration
///////////////////////////////////////////////////////////////////////////

NProgress.configure({ showSpinner: true, easing: "ease", speed: 500 }); // NProgress Configuration
NProgress.start();

var uri = "";

var Axios = axios.create({
  baseURL: "https://www.phiscus.online/",
  withCredentials: true,
  headers: {
    "Content-type": "*/*",
    Authorization: `Bearer ${getToken()}`
  }
});

//This allows you to intercept the request before it is sent and alter headers or anyting else that is passed to the axios config.
Axios.interceptors.request.use(
  config => {
    uri = config.url;
    return config;
  },
  error => {
    console.log("Interceptor Request Error" + error);
  }
);

//This allows you to intercept the response and check the status and error messages and if ncessary reject the promise.
Axios.interceptors.response.use(
  // eslint-disable-next-line no-unused-vars
  response => {
    return ApiCall(uri);
  },
  error => {
    console.log("Interceptor Response Error" + error);
  }
);

NProgress.done();
export default Axios;
