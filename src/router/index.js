import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store";
// Additionals
import { isLoggedIn } from "@/utils/auth";
import NProgress from "nprogress"; // progress bar
import "nprogress/nprogress.css"; // progress bar style

import defaultSettings from "@/system/settings";
const { title } = defaultSettings;
// End Additionals

Vue.use(VueRouter);

import constR from "@/router/modules/const-routes";
import asyncR from "@/router/modules/async-routes";

let routes = [].concat(constR, asyncR);

var router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: routes
});

const createRouter = () =>
  new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    scrollBehavior: (to, from, savedPosition) => {
      if (to.hash) return { selector: to.hash };
      if (savedPosition) return savedPosition;

      return { x: 0, y: 0 };
    },
    routes: routes
  });

///////////////////////////////////////////////////////////////////////////////////////
export const constRoutes = constR;
export const asyncRoutes = asyncR;
// // Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}
//////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/////////  Router Configurations
/////////  Permissions Routing
/////////  NProgress - (Loader)
/////////  Role routing Specific
/////////  Basically, Its a smart Routing system configuration
///////////////////////////////////////////////////////////////////////////

NProgress.configure({ showSpinner: true, easing: "ease", speed: 500 }); // NProgress Configuration

router.beforeEach(async (to, from, next) => {
  // start progress bar
  NProgress.start();
  // set page title
  document.title = title;
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!isLoggedIn()) {
      next({
        path: "/G/",
        query: { redirect: to.fullPath }
      });
    } else {
      console.log("logged in user & permissions");
      // Remove sidebar activator
      store.dispatch("settings/haschildren", false);
      // Get user logged in information
      await store.dispatch("user/userinfo");
      next();
    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (isLoggedIn()) {
      // If user is logged in -> dashboard
      // Remove sidebar activator
      store.dispatch("settings/haschildren", false);
      next({
        path: "/dashboard",
        query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
  } else {
    next(); // make sure to always call next()!
  }
});

router.afterEach(() => {
  // finish progress bar
  NProgress.done();
});

export default router;
