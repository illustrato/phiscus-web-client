///////////////////////////////////////////////
//### Layouts
////////////////////////////////////////
import AdminLayout from "@/layouts/admin";
import E404 from "@/views/errors/404";

const routes = [
  //#######
  //####### Error
  //#######
  {
    path: "/404",
    name: "404",
    component: E404,
    meta: {
      requiresAuth: false,
      layout: AdminLayout,
      title: "404",
      hidden: true
    }
  },
  { path: "/*", redirect: "/G/FourOhFour", hidden: true }
];

export default routes;
