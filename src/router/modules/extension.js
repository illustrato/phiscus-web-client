///////////////////////////////////////////////
//### Layouts
////////////////////////////////////////
import AdminLayout from "@/layouts/admin";
import Extension from "@/views/extension";

const routes = [
  //#######
  //####### Extension
  //#######
  {
    path: "/extensions",
    name: "extensions",
    component: Extension,
    meta: {
      slug: "extensions",
      requiresAuth: true,
      layout: AdminLayout,
      title: "Modules",
      icon: "mdi-puzzle",
      roles: ["admin", "manager"],
      permissions: ["rw", "rw"],
      description: "Modules",
      breadCrumb: [
        {
          to: "#",
          text: "Modules"
        }
      ]
    },
    children: [
      {
        path: ":id",
        name: "read-extension",
        component: Extension,
        meta: {
          slug: "read-extension",
          requiresAuth: true,
          layout: AdminLayout,
          title: "Module",
          icon: "mdi-puzzle",
          roles: ["admin", "manager"],
          permissions: ["rw", "rw"],
          description: "Module Item",
          breadCrumb: [
            {
              to: "/extensions",
              text: "Modules"
            },
            {
              to: "#",
              text: "Module"
            }
          ],
          hidden: true
        }
      }
    ]
  }
];

export default routes;
