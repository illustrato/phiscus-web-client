///////////////////////////////////////////////
//### Layouts
////////////////////////////////////////
import AdminLayout from "@/layouts/admin";
import Shop from "@/views/modules/shop";

const routes = [
  //#######
  //####### Shop
  //#######
  {
    path: "/shop",
    redirect: "/shop/sale",
    name: "shop",
    component: Shop,
    meta: {
      slug: "shop",
      parent: "",
      requiresAuth: true,
      layout: AdminLayout,
      title: "Shop",
      icon: "mdi-store",
      roles: ["admin"],
      permissions: ["rw"],
      description: "Shop",
      breadCrumb: [
        {
          to: "#",
          text: "shop"
        }
      ]
    },
    children: [
      {
        path: "/shop/sale",
        name: "shop-sale",
        component: Shop,
        meta: {
          slug: "sale",
          parent: "shop",
          requiresAuth: true,
          icon: "mdi-sale",
          layout: AdminLayout,
          title: "Sale",
          description: "Sales",
          breadCrumb: [
            {
              to: "/shop",
              text: "Shop"
            },
            {
              to: "#",
              text: "Sale"
            }
          ]
        },
        children: [
          {
            path: "/shop/sale/read/:id",
            name: "read-sale",
            component: Shop,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Read Sale",
              hidden: true,
              slug: "sale",
              parent: "sale",
              breadCrumb: [
                {
                  to: "/shop",
                  text: "Shop"
                },
                {
                  to: "/shop/sale",
                  text: "Sales"
                },
                {
                  to: "#",
                  text: "Sale Details"
                }
              ]
            }
          },
          {
            path: "/shop/sale/edit/:id",
            name: "edit-sale",
            component: Shop,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Edit Sale",
              hidden: true,
              slug: "sale",
              parent: "sale",
              breadCrumb: [
                {
                  to: "/shop",
                  text: "Shop"
                },
                {
                  to: "/shop/sale",
                  text: "Sales"
                },
                {
                  to: "#",
                  text: "Edit Sale"
                }
              ]
            }
          },
          {
            path: "/shop/sale/add",
            name: "add-sale",
            redirect: "/pos",
            component: Shop,
            meta: {
              requiresAuth: true,
              // layout: AdminLayout,
              title: "Add Sale",
              hidden: true,
              slug: "sale",
              parent: "sale",
              // breadCrumb: [
              //   {
              //     to: "/shop",
              //     text: "Shop"
              //   },
              //   {
              //     to: "/shop/sale",
              //     text: "Sales"
              //   },
              //   {
              //     to: "#",
              //     text: "New Sale"
              //   }
              // ]
            }
          }
        ]
      }
    ]
  },
];

export default routes;
