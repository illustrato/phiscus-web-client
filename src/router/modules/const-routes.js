const constRoutes = [
  { path: "/", redirect: "/dashboard", hidden: true },
  {
    path: "/G/",
    component: () => import("@/layouts/home/Index.vue"),
    children: [
      {
        path: "",
        name: "Home",
        component: () => import("@/views/guest/home/Index.vue")
      },
      {
        path: "about",
        name: "About",
        component: () => import("@/views/guest/about/Index.vue"),
        meta: { src: require("@/assets/about.jpg") }
      },
      {
        path: "contact-us",
        name: "Contact",
        component: () => import("@/views/guest/contact-us/Index.vue"),
        meta: { src: require("@/assets/contact.jpg") }
      },
      {
        path: "features",
        name: "Features",
        component: () => import("@/views/guest/pro/Index.vue"),
        meta: { src: require("@/assets/pro.jpg") }
      },
      {
        path: "register",
        name: "Register",
        component: () => import("@/views/guest/register/Index.vue"),
        meta: { src: require("@/assets/pro.jpg") }
      },
      {
        path: "login",
        name: "Login",
        component: () => import("@/views/guest/login/Index.vue"),
        meta: { src: require("@/assets/pro.jpg") }
      },
      {
        path: "verify-email",
        name: "VerifyEmail",
        component: () => import("@/views/guest/verify-email/Index.vue")
      },
      {
        path: "/G/*",
        name: "FourOhFour",
        component: () => import("@/views/guest/404/Index.vue")
      }
    ]
  }
];

export default constRoutes;
