///////////////////////////////////////////////
//### Layouts
////////////////////////////////////////
import AdminLayout from "@/layouts/admin";
import Help from "@/views/help";

const routes = [
  //#######
  //####### Help
  //#######
  {
    path: "/help",
    redirect: "/help/getting-started",
    name: "help",
    component: Help,
    meta: {
      slug: "help",
      requiresAuth: true,
      layout: AdminLayout,
      title: "Help",
      icon: "mdi-help-circle-outline",
      roles: ["admin"],
      permissions: ["rw"],
      description: "Help",
      breadCrumb: [
        {
          to: "#",
          text: "Help"
        }
      ]
    },
    children: [
      {
        path: "getting-started",
        name: "help-getting-started",
        component: Help,
        meta: {
          requiresAuth: true,
          icon: "mdi-clock-start",
          layout: AdminLayout,
          title: "Getting Started",
          description: "Getting Started",
          breadCrumb: [
            {
              to: "/help",
              text: "Help"
            },
            {
              to: "#",
              text: "Getting Started"
            }
          ]
        }
      },
      {
        path: "how-to",
        name: "help-how-to",
        component: Help,
        meta: {
          requiresAuth: true,
          icon: "mdi-human-baby-changing-table",
          layout: AdminLayout,
          title: "How To",
          description: "How to ....",
          breadCrumb: [
            {
              to: "/help",
              text: "Help"
            },
            {
              to: "#",
              text: "How To"
            }
          ]
        }
      },
      {
        path: "videos",
        name: "help-videos",
        component: Help,
        meta: {
          requiresAuth: true,
          icon: "mdi-video",
          layout: AdminLayout,
          title: "Help Videos",
          description: "Help videos",
          breadCrumb: [
            {
              to: "/help",
              text: "Help"
            },
            {
              to: "#",
              text: "Videos"
            }
          ]
        }
      },
      {
        path: "faq",
        name: "help-faq",
        component: Help,
        meta: {
          requiresAuth: true,
          icon: "mdi-progress-question",
          layout: AdminLayout,
          title: "FAQ",
          description: "FAQ",
          breadCrumb: [
            {
              to: "/help",
              text: "Help"
            },
            {
              to: "#",
              text: "FAQ"
            }
          ]
        }
      }
    ]
  }
];

export default routes;
