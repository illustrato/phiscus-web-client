///////////////////////////////////////////////
//### Layouts
////////////////////////////////////////
import AdminLayout from "@/layouts/admin";
import Settings from "@/views/settings";

const routes = [
  //#######
  //####### Settings
  //#######
  {
    path: "/settings",
    redirect: "/settings/about",
    name: "settings",
    component: Settings,
    meta: {
      slug: "settings",
      requiresAuth: true,
      layout: AdminLayout,
      title: "Settings",
      icon: "mdi-cog",
      roles: ["admin"],
      permissions: ["rw"],
      description: "Settings",
      breadCrumb: [
        {
          to: "#",
          text: "Settings"
        }
      ]
    },
    children: [
      {
        path: "about",
        name: "settings-about",
        component: Settings,
        meta: {
          requiresAuth: true,
          icon: "mdi-information-variant",
          layout: AdminLayout,
          title: "About",
          description: "About Phiscus",
          breadCrumb: [
            {
              to: "/settings",
              text: "Settings"
            },
            {
              to: "#",
              text: "About"
            }
          ]
        }
      },
      {
        path: "theme",
        name: "settings-theme",
        component: Settings,
        meta: {
          requiresAuth: true,
          icon: "mdi-theme-light-dark",
          layout: AdminLayout,
          title: "Theme",
          description: "Theme",
          breadCrumb: [
            {
              to: "/settings",
              text: "Settings"
            },
            {
              to: "#",
              text: "Theme"
            }
          ]
        }
      },
      {
        path: "language",
        name: "settings-language",
        component: Settings,
        meta: {
          requiresAuth: true,
          icon: "mdi-earth",
          layout: AdminLayout,
          title: "Language",
          description: "Language",
          breadCrumb: [
            {
              to: "/settings",
              text: "Settings"
            },
            {
              to: "#",
              text: "Language"
            }
          ]
        }
      },
      {
        path: "region",
        name: "settings-region",
        component: Settings,
        meta: {
          requiresAuth: true,
          icon: "mdi-island",
          layout: AdminLayout,
          title: "Region & Time",
          description: "Region",
          breadCrumb: [
            {
              to: "/settings",
              text: "Settings"
            },
            {
              to: "#",
              text: "Region"
            }
          ]
        }
      }
    ]
  }
];

export default routes;
