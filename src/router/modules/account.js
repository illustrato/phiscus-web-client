///////////////////////////////////////////////
//### Layouts
////////////////////////////////////////
import AdminLayout from "@/layouts/admin";
import Account from "@/views/account";

const routes = [
  //#######
  //####### Account
  //#######
  {
    path: "/account",
    redirect: "/account/personal",
    name: "account",
    component: Account,
    meta: {
      slug: "account",
      requiresAuth: true,
      layout: AdminLayout,
      title: "Account",
      icon: "mdi-account",
      roles: ["admin"],
      permissions: ["rw"],
      description: "Account",
      breadCrumb: [
        {
          to: "#",
          text: "Account"
        }
      ]
    },
    children: [
      {
        path: "avatar",
        name: "account-avatar",
        component: Account,
        meta: {
          requiresAuth: true,
          icon: "mdi-picture-in-picture-bottom-right",
          layout: AdminLayout,
          title: "Avatar",
          description: "Avatar",
          breadCrumb: [
            {
              to: "/account",
              text: "Account"
            },
            {
              to: "#",
              text: "Avatar"
            }
          ]
        }
      },
      {
        path: "personal",
        name: "account-personal",
        component: Account,
        meta: {
          requiresAuth: true,
          icon: "mdi-bag-personal",
          layout: AdminLayout,
          title: "Personal",
          description: "Personal",
          breadCrumb: [
            {
              to: "/account",
              text: "Account"
            },
            {
              to: "#",
              text: "Personal"
            }
          ]
        }
      },
      {
        path: "contact",
        name: "account-contact",
        component: Account,
        meta: {
          requiresAuth: true,
          icon: "mdi-card-account-phone",
          layout: AdminLayout,
          title: "Contact",
          description: "Contact",
          breadCrumb: [
            {
              to: "/account",
              text: "Account"
            },
            {
              to: "#",
              text: "Contact"
            }
          ]
        }
      },
      {
        path: "subscription",
        name: "account-subscription",
        component: Account,
        meta: {
          requiresAuth: true,
          icon: "mdi-room-service-outline",
          layout: AdminLayout,
          title: "Subscription & Services",
          description: "Subscription",
          breadCrumb: [
            {
              to: "/account",
              text: "Account"
            },
            {
              to: "#",
              text: "Subscription & Services"
            }
          ]
        }
      },
      {
        path: "payment",
        name: "account-payment",
        component: Account,
        meta: {
          requiresAuth: true,
          icon: "mdi-contactless-payment-circle",
          layout: AdminLayout,
          title: "Payment Methods",
          description: "Account Payment",
          breadCrumb: [
            {
              to: "/account",
              text: "Account"
            },
            {
              to: "/account/subscription",
              text: "Subscription"
            },
            {
              to: "#",
              text: "Pay Methods"
            }
          ]
        }
      },
      {
        path: "address",
        name: "account-address",
        component: Account,
        meta: {
          requiresAuth: true,
          icon: "mdi-map-marker-circle",
          layout: AdminLayout,
          title: "Address",
          description: "Account Address",
          breadCrumb: [
            {
              to: "/account",
              text: "Account"
            },
            {
              to: "/account/subscription",
              text: "Subscription"
            },
            {
              to: "#",
              text: "Address"
            }
          ]
        }
      },
      {
        path: "transaction",
        name: "account-transaction",
        component: Account,
        meta: {
          requiresAuth: true,
          icon: "mdi-handshake",
          layout: AdminLayout,
          title: "Transaction",
          description: "Account Transaction",
          breadCrumb: [
            {
              to: "/account",
              text: "Account"
            },
            {
              to: "/account/subscription",
              text: "Subscription"
            },
            {
              to: "#",
              text: "Transactions"
            }
          ]
        }
      }
    ]
  }
];

export default routes;
