///////////////////////////////////////////////
//### Layouts
////////////////////////////////////////
import AdminLayout from "@/layouts/admin";
import Dashboard from "@/views/dashboard";

const routes = [
  //#######
  //####### Dashboard
  //#######
  {
    path: "/dashboard",
    name: "dashboard",
    component: Dashboard,
    meta: {
      slug: "dashboard",
      requiresAuth: true,
      layout: AdminLayout,
      title: "Dashboard",
      icon: "mdi-view-dashboard",
      roles: ["admin", "manager"],
      permissions: ["rw", "rw"],
      description: "Landing and a few graphs",
      breadCrumb: [
        {
          to: "#",
          text: "Dashboard"
        }
      ]
    }
  }
];

export default routes;
