import Errors from "./errors";
import Dashboard from "./dashboard";
import Account from "./account";
import Extension from "./extension";
import Help from "./help";
import Notification from "./notification";
import Settings from "./settings";
import Support from "./support";
import Catalog from "./catalog";
import Tariff from "./tariff";
import HR from "./hr";
import POS from "./pos";
import Shop from "./shop";

const asyncRoutes = [].concat(Dashboard, Catalog, HR, Tariff, POS, Shop, Extension, Notification, Account, Settings, Help, Support, Errors);

export default asyncRoutes;
