///////////////////////////////////////////////
//### Layouts
////////////////////////////////////////
import AdminLayout from "@/layouts/admin";
import Tariff from "@/views/modules/tariff";

const routes = [
  //#######
  //####### Account
  //#######
  {
    path: "/tariff",
    redirect: "/tariff/tax",
    name: "tariff",
    component: Tariff,
    meta: {
      slug: "tariff",
      parent: "",
      requiresAuth: true,
      layout: AdminLayout,
      title: "Tariff",
      icon: "mdi-wallet-outline",
      roles: ["admin"],
      permissions: ["rw"],
      description: "Tariff",
      breadCrumb: [
        {
          to: "#",
          text: "Tariff"
        }
      ]
    },
    children: [
      {
        path: "/tariff/tax",
        name: "tax",
        component: Tariff,
        meta: {
          slug: "tax",
          parent: "tariff",
          requiresAuth: true,
          icon: "mdi-cash-minus",
          layout: AdminLayout,
          title: "Tax",
          description: "Currency to which your system is to use for pricing and payment transactions",
          breadCrumb: [
            {
              to: "/tariff",
              text: "Tariff"
            },
            {
              to: "#",
              text: "Taxes"
            }
          ]
        },
        children: [
          {
            path: "/tariff/tax/read/:id",
            name: "read-tax",
            component: Tariff,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Read Tax",
              hidden: true,
              slug: "tax",
              parent: "tax",
              breadCrumb: [
                {
                  to: "/tariff",
                  text: "Tariff"
                },
                {
                  to: "/tariff/tax",
                  text: "Taxes"
                },
                {
                  to: "#",
                  text: "Read Tax"
                }
              ]
            }
          },
          {
            path: "/tariff/tax/edit/:id",
            name: "edit-tax",
            component: Tariff,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Edit Tax",
              hidden: true,
              slug: "tax",
              parent: "tax",
              breadCrumb: [
                {
                  to: "/tariff",
                  text: "Tariff"
                },
                {
                  to: "/tariff/tax",
                  text: "Taxes"
                },
                {
                  to: "#",
                  text: "Edit Tax"
                }
              ]
            }
          },
          {
            path: "/tariff/tax/add",
            name: "add-tax",
            component: Tariff,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Add Tax",
              hidden: true,
              slug: "tax",
              parent: "tax",
              breadCrumb: [
                {
                  to: "/tariff",
                  text: "Tariff"
                },
                {
                  to: "/tariff/tax",
                  text: "Taxes"
                },
                {
                  to: "#",
                  text: "Add Tax"
                }
              ]
            }
          }
        ]
      },
      {
        path: "/tariff/currency",
        name: "currency",
        component: Tariff,
        meta: {
          slug: "currency",
          parent: "tariff",
          requiresAuth: true,
          icon: "mdi-currency-usd-circle-outline",
          layout: AdminLayout,
          title: "Currency",
          description: "Currency to which your system is to use for pricing and payment transactions",
          breadCrumb: [
            {
              to: "/tariff",
              text: "Tariff"
            },
            {
              to: "#",
              text: "Currency"
            }
          ]
        },
        children: [
          {
            path: "/tariff/currency/read/:id",
            name: "read-currency",
            component: Tariff,
            parent: "tariff",
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Read Currency",
              hidden: true,
              slug: "currency",
              parent: "currency",
              breadCrumb: [
                {
                  to: "/tariff",
                  text: "Tariff"
                },
                {
                  to: "/tariff/currency",
                  text: "Currencies"
                },
                {
                  to: "#",
                  text: "Read Currency"
                }
              ]
            }
          },
          {
            path: "/tariff/currency/edit/:id",
            name: "edit-currency",
            component: Tariff,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Edit Currency",
              hidden: true,
              slug: "currency",
              parent: "currency",
              breadCrumb: [
                {
                  to: "/tariff",
                  text: "Tariff"
                },
                {
                  to: "/tariff/currency",
                  text: "Currencies"
                },
                {
                  to: "#",
                  text: "Edit Currency"
                }
              ]
            }
          },
          {
            path: "/tariff/currency/add",
            name: "add-currency",
            component: Tariff,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Add Currency",
              hidden: true,
              slug: "currency",
              parent: "currency",
              breadCrumb: [
                {
                  to: "/tariff",
                  text: "Tariff"
                },
                {
                  to: "/tariff/currency",
                  text: "Currencies"
                },
                {
                  to: "#",
                  text: "Add Currency"
                }
              ]
            }
          }
        ]
      }
    ]
  }
];

export default routes;
