///////////////////////////////////////////////
//### Layouts
////////////////////////////////////////
import AdminLayout from "@/layouts/admin";
import Notification from "@/views/notification";

const routes = [
  //#######
  //####### Notification
  //#######
  {
    path: "/notification",
    name: "notification",
    component: Notification,
    meta: {
      slug: "notification",
      requiresAuth: true,
      layout: AdminLayout,
      title: "Notifications",
      icon: "mdi-bell-circle-outline",
      roles: ["admin", "manager"],
      permissions: ["rw", "rw"],
      description: "Notifications",
      breadCrumb: [
        {
          to: "#",
          text: "Notification"
        }
      ]
    }
  }
];

export default routes;
