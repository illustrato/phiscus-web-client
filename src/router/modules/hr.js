///////////////////////////////////////////////
//### Layouts
////////////////////////////////////////
import AdminLayout from "@/layouts/admin";
import HR from "@/views/modules/hr";

const routes = [
  //#######
  //####### Extension
  //#######
  {
    path: "/hr",
    name: "hr",
    redirect: "/hr/employee",
    component: HR,
    meta: {
      slug: "hr",
      requiresAuth: true,
      layout: AdminLayout,
      title: "Human Resource",
      icon: "mdi-human-male-height",
      roles: ["admin", "manager"],
      permissions: ["rw", "rw"],
      description: "Human Resources",
      breadCrumb: [
        {
          to: "#",
          text: "Human Resource"
        }
      ]
    },
    children: [
      {
        path: "/hr/employee",
        name: "employee",
        component: HR,
        meta: {
          slug: "employee",
          parent: "hr",
          requiresAuth: true,
          icon: "mdi-account-tie",
          layout: AdminLayout,
          title: "Employee",
          description: "Store Employee",
          breadCrumb: [
            {
              to: "/hr",
              text: "HR"
            },
            {
              to: "#",
              text: "Employees"
            }
          ]
        },
        children: [
          {
            path: "/hr/employee/read/:id",
            name: "read-employee",
            component: HR,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Read Employee",
              hidden: true,
              slug: "employee",
              parent: "employee",
              breadCrumb: [
                {
                  to: "/hr",
                  text: "HR"
                },
                {
                  to: "/hr/employee",
                  text: "Employees"
                },
                {
                  to: "#",
                  text: "Employee Details"
                }
              ]
            }
          },
          {
            path: "/hr/employee/edit/:id",
            name: "edit-employee",
            component: HR,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Edit Employee",
              hidden: true,
              slug: "employee",
              parent: "employee",
              breadCrumb: [
                {
                  to: "/hr",
                  text: "HR"
                },
                {
                  to: "/hr/employee",
                  text: "Employees"
                },
                {
                  to: "#",
                  text: "Edit Employee"
                }
              ]
            }
          },
          {
            path: "/hr/employee/add",
            name: "add-employee",
            component: HR,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Add Employee",
              hidden: true,
              slug: "employee",
              parent: "employee",
              breadCrumb: [
                {
                  to: "/hr",
                  text: "HR"
                },
                {
                  to: "/hr/employee",
                  text: "Employees"
                },
                {
                  to: "#",
                  text: "New Employee"
                }
              ]
            }
          }
        ]
      }
    ]
  }
];

export default routes;
