///////////////////////////////////////////////
//### Layouts
////////////////////////////////////////
import AdminLayout from "@/layouts/admin";
import Support from "@/views/support";

const routes = [
  //#######
  //####### Support
  //#######
  {
    path: "/support",
    redirect: "/support/oliver",
    name: "support",
    component: Support,
    meta: {
      slug: "support",
      requiresAuth: true,
      layout: AdminLayout,
      title: "Support",
      icon: "mdi-headset",
      roles: ["admin"],
      permissions: ["rw"],
      description: "Support",
      breadCrumb: [
        {
          to: "#",
          text: "support"
        }
      ]
    },
    children: [
      {
        path: "oliver",
        name: "support-oliver",
        component: Support,
        meta: {
          requiresAuth: true,
          icon: "mdi-robot",
          layout: AdminLayout,
          title: "Oliver",
          description: "AI Contact",
          breadCrumb: [
            {
              to: "/support",
              text: "Support"
            },
            {
              to: "#",
              text: "AI Bot - Oliver"
            }
          ]
        }
      },
      {
        path: "contact",
        name: "support-contact",
        component: Support,
        meta: {
          requiresAuth: true,
          icon: "mdi-phone-in-talk",
          layout: AdminLayout,
          title: "Contact",
          description: "Human Interaction",
          breadCrumb: [
            {
              to: "/support",
              text: "Support"
            },
            {
              to: "#",
              text: "Human Interaction"
            }
          ]
        }
      }
    ]
  }
];

export default routes;
