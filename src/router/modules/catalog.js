///////////////////////////////////////////////
//### Layouts
////////////////////////////////////////
import AdminLayout from "@/layouts/admin";
import Catalog from "@/views/modules/catalog";

const routes = [
  //#######
  //####### Account
  //#######
  {
    path: "/catalog",
    redirect: "/catalog/product",
    name: "catalog",
    component: Catalog,
    meta: {
      slug: "catalog",
      parent: "",
      requiresAuth: true,
      layout: AdminLayout,
      title: "Catalog",
      icon: "mdi-book-open-page-variant",
      roles: ["admin"],
      permissions: ["rw"],
      description: "Catalog",
      breadCrumb: [
        {
          to: "#",
          text: "Catalog"
        }
      ]
    },
    children: [
      {
        path: "/catalog/product",
        name: "product",
        component: Catalog,
        meta: {
          slug: "product",
          parent: "catalog",
          requiresAuth: true,
          icon: "mdi-food-fork-drink",
          layout: AdminLayout,
          title: "Product",
          description: "Product",
          breadCrumb: [
            {
              to: "/catalog",
              text: "Catalog"
            },
            {
              to: "#",
              text: "Products"
            }
          ]
        },
        children: [
          {
            path: "/catalog/product/read/:id",
            name: "read-product",
            component: Catalog,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Read Product",
              hidden: true,
              slug: "product",
              parent: "product",
              breadCrumb: [
                {
                  to: "/catalog",
                  text: "Catalog"
                },
                {
                  to: "/catalog/product",
                  text: "Products"
                },
                {
                  to: "#",
                  text: "Read Product"
                }
              ]
            }
          },
          {
            path: "/catalog/product/edit/:id",
            name: "edit-product",
            component: Catalog,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Edit Product",
              hidden: true,
              slug: "product",
              parent: "product",
              breadCrumb: [
                {
                  to: "/catalog",
                  text: "Catalog"
                },
                {
                  to: "/catalog/product",
                  text: "Products"
                },
                {
                  to: "#",
                  text: "Edit Product"
                }
              ]
            }
          },
          {
            path: "/catalog/product/add",
            name: "add-product",
            component: Catalog,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Add Product",
              hidden: true,
              slug: "product",
              parent: "product",
              breadCrumb: [
                {
                  to: "/catalog",
                  text: "Catalog"
                },
                {
                  to: "/catalog/product",
                  text: "Products"
                },
                {
                  to: "#",
                  text: "Add Product"
                }
              ]
            }
          }
        ]
      },
      {
        path: "/catalog/category",
        name: "category",
        component: Catalog,
        meta: {
          slug: "category",
          parent: "catalog",
          requiresAuth: true,
          icon: "mdi-cards-playing-outline",
          layout: AdminLayout,
          title: "Category",
          description: "Category to which your system is to use for pricing and payment transactions",
          breadCrumb: [
            {
              to: "/catalog",
              text: "Catalog"
            },
            {
              to: "#",
              text: "Category"
            }
          ]
        },
        children: [
          {
            path: "/catalog/category/read/:id",
            name: "read-category",
            component: Catalog,
            parent: "catalog",
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Read Category",
              hidden: true,
              slug: "category",
              parent: "category",
              breadCrumb: [
                {
                  to: "/catalog",
                  text: "Catalog"
                },
                {
                  to: "/catalog/category",
                  text: "Categories"
                },
                {
                  to: "#",
                  text: "Read Category"
                }
              ]
            }
          },
          {
            path: "/catalog/category/edit/:id",
            name: "edit-category",
            component: Catalog,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Edit Category",
              hidden: true,
              slug: "category",
              parent: "category",
              breadCrumb: [
                {
                  to: "/catalog",
                  text: "Catalog"
                },
                {
                  to: "/catalog/category",
                  text: "Categories"
                },
                {
                  to: "#",
                  text: "Edit Category"
                }
              ]
            }
          },
          {
            path: "/catalog/category/add",
            name: "add-category",
            component: Catalog,
            meta: {
              requiresAuth: true,
              layout: AdminLayout,
              title: "Add Category",
              hidden: true,
              slug: "category",
              parent: "category",
              breadCrumb: [
                {
                  to: "/catalog",
                  text: "Catalog"
                },
                {
                  to: "/catalog/category",
                  text: "Categories"
                },
                {
                  to: "#",
                  text: "Add Category"
                }
              ]
            }
          }
        ]
      }
    ]
  }
];

export default routes;
