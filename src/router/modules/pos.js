///////////////////////////////////////////////
//### Layouts
////////////////////////////////////////
import POSLayout from "@/layouts/admin/POSLayout";
import InStorePOS from "@/views/modules/shop/pos";

const routes = [
  //#######
  //####### Point of Sale
  //#######
  {
    path: "/pos",
    name: "instore-pos",
    component: InStorePOS,
    meta: {
      slug: "pos",
      parent: "",
      requiresAuth: true,
      layout: POSLayout,
      title: "POS",
      icon: "mdi-point-of-sale",
      roles: ["admin"],
      permissions: ["rw"],
      description: "POS",
      breadCrumb: [
        {
          to: "#",
          text: "POS"
        }
      ]
    }
  }
];

export default routes;
