# Phiscus Web Front End

## Expectations
- Catalog
  - Category
  - Products

- Inventory
  - Stock
  - Incoming
  - Arrived
  - Low Quantity
  - Medium Quantity
  - Full Quantity
  - Damaged / Expired
  - Broken - Customer or employee breakage
  - Good Condition

- Orders
  - Auto Orders
  - Closed Orders
  - Active Orders
  - Cancelled Orders

- Shop
  - In-Store POS
  - Sales Special / Promotion
  - Sales Success
  - Sales Pending
  - Sales Failed

- Tariff
  - Taxes
    - VAT
  - Currencies

- Reports
  - Sales
    - Most Sold
    - Least Sold
    - Profit
    - Gross
    - Loss - By Special/Promotion
  - Stock Left
    - Expected Profit
    - Expected Expenses - Re-order
    - Expected Gross
  - Stock Broken
    - Loss
  - Stock Damaged
    - Loss 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
