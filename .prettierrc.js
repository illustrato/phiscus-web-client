module.exports = {
  singleQuote: false,
  semi: true,
  printWidth: 140,
  jsxBracketSameLine: true,
  trim_trailing_whitespace: true,
  insert_final_newline: true,
  tabWidth: 2,
  trailingComma: "none",
}
